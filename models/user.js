const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    requited: true,
  },
  cart: {
    items: [
      {
        prodId: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true
        },
        qty: {
          type: Number,
          requited: true
        }
      }
    ]
  }

});

userSchema.methods.getCartItems = function() {
  return this.cart;
}

userSchema.methods.updateCart = function(prodId, cart) {
  let updatedCart = {};
  if (!cart || !cart.items) {
    updatedCart.items[0] = {prodId: prodId, qty: 1};
  } else {
    // do not match on type as in "==="
    updatedCart = {...cart};
    let index = cart.items.findIndex(
      item => item.prodId == prodId
    );
    if (index > -1) {
      let qty = +cart.items[index].qty;
      updatedCart.items[index] = {prodId: prodId, qty: ++qty};
    } else {
      updatedCart.items.push( {
        prodId: prodId, 
        qty: 1
      });
    }
  }
  cart = updatedCart;
  return this.save();
  
}

module.exports = mongoose.model('User', userSchema);

