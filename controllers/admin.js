const Product = require('../models/product');

exports.getProducts = (req, res, next) => {
  Product.find()
    // .select('title qty')
    // .populate('userId', 'name')
    .then( products => {
      res.render('admin/products', {
        pageTitle: 'All Products',
        path: '/admin/products',
        editing: false,
        prods: products
      });
    })
    .catch(err => {
      console.log("TCL: exports.getProducts -> err", err)
    })
}

exports.getAddProduct = (req, res, next) => {
  res.render('admin/edit-product', {
    pageTitle: 'Add Product',
    path: '/admin/add-product',
    editing: false
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const price = req.body.price;
  const imageUrl = req.body.imageUrl;
  const desc = req.body.desc;
  const userId = req.user._id;
  
  const product = new Product({
    title: title,
    price: price,
    imageUrl: imageUrl,
    desc: desc,
    userId: userId
  });
  
  product.save()
    .then(result => {
      res.redirect('/admin/products');
    })
    .catch(err => {
      console.log(err);
    });
};

exports.getEditProduct = (req, res, next) => {
  const prodId = req.params.productId;

  // mongoose converts string to objectId
  Product.findById(prodId)
    .then( product => {
      res.render('admin/edit-product', {
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        editing: true,
        product: product
      });
    })
    .catch( err => {
      console.log("TCL: exports.getEditProduct -> err", err)
    })
}

exports.postEditProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const updatedImageUrl = req.body.imageUrl;
  const updatedDesc = req.body.desc;
 
  Product.findById(prodId)
    .then(prod => {
      prod.title = updatedTitle;
      prod.price = updatedPrice;
      prod.imageUtl = updatedImageUrl;
      prod.desc = updatedDesc;

      return prod.save();
    })
    .then(result => {
      res.redirect('/admin/products');
    })
    .catch(err => {
      console.log("TCL: exports.postEditProduct -> err", err)
    })
};

exports.postDeleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  Product.findByIdAndRemove(prodId)
    .then(result => {
      // result is an object being removed     
      res.redirect('/admin/products');
    })
    .catch(error => {
      console.log("TCL: exports.postDeleteProduct -> error", error)
    })
}














